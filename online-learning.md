Below is a list of course materials, interactive online training resources, and other learning tools for computational research skills and bioinformatics. We hope will be helpful to those members of the EMBL community who can't access their research or work while Covid-19-related restrictions are in place. These resources have been recommended by members of the Bio-IT community.

## Course materials

* [Bio-IT course materials page](/course-materials/) - includes fundamental skills (command line, cluster, programming etc), Centres material (network analysis, statistics) and specialised topics (image analysis).
* [GBCS course material](https://gbservices.embl.de/Tutorials/Git_and_Conda_Training/) - training material from Genome Biology Computational Support, covering Git for version control and Conda for package/environment management. (Requires VPN access to EMBL network.)
* [EMBL Rome Bioinformatics training page](https://romebioinfo.embl-community.io/romebioinfo.embl-community.io/training/) - upcoming courses and links to course materials for computational training provided by Nicolas Descostes at EMBL Rome
* [EMBL-EBI Train Online](https://www.ebi.ac.uk/training/online/ ) - free access to a comprehensive collection of online bioinformatics courses
* [Galaxy Training](https://training.galaxyproject.org/) - very comprehensive resource for learning reproducible data analysis with Galaxy, from introduction to ChIP-seq data analysis. You can [follow a previous training](https://stocks.embl.de/assays/experiments/entry/db049718-2778-424e-897d-2083ebea1011) and get started on [https://usegalaxy.eu](https://usegalaxy.eu). For your own data we recommend to switch to [GBCS’ Galaxy instance](https://galaxy.embl.de) and follow [their presentation](https://stocks.embl.de/apps/office/510c666e-f162-4f89-b4a2-141402f1094f). (Requires VPN access to EMBL network.)
* [CodeAcademy](https://www.codecademy.com/) - large catalogue of interactive online courses for skills essential to computational research, including programming languages (e.g. R, Python), command line, version control. Sign-up required, some courses require payment.
* [Software Carpentry](https://software-carpentry.org/lessons/) - free and accessible lesson material covering essential computational research skills. Software Carpentry teaches better software development skills for researchers and includes lessons on Bash, R, Python, Git, SQL, and Make.
* [Data Carpentry](https://datacarpentry.org/lessons/) - teaches better practices in data handling and analysis, including lessons on spreadsheets, R, Python, OpenRefine, Cloud Computing, Bash, and more. One Data Carpentry curriculum is specifically aimed at researchers [working with genomics data](https://datacarpentry.org/lessons/#genomics-workshop), another at researchers learning [image processing with Python](https://datacarpentry.org/image-processing/) (co-authored by members of Anna Kreshuk’s group at EMBL). Lastly, Data Carpentry also provides material for a [semester-long course for biologists](https://datacarpentry.org/semester-biology/schedule/), originally taught by Ethan White at the University of Florida.
* [Aaron Quinlan’s bioinformatics tutorials](http://quinlanlab.org/) - two lessons on core skills in genomics, using bedtools to [work with genomic interval files](http://quinlanlab.org/tutorials/bedtools/bedtools.html), and using [samtools to work with SAM/BAM alignment files](http://quinlanlab.org/tutorials/samtools/samtools.html).

## Webinars & virtual courses

* [Bio-IT & Centres training courses](https://bio-it.embl.de/upcoming-courses/)
* [EMBL-EBI Webinars](https://www.ebi.ac.uk/training/webinars) - register here for live webinars focused on EBI resources. Recordings of previous webinars can be found on Train Online (link above)
* [BioExcel](https://bioexcel.eu/services/training/) - virtual summer school and webinars focused on the use of HPC for modelling and simulation
* [CINECA webinars](https://www.cineca-project.eu/) - webinars focused on sharing patient data across borders
* [NEUBIAS Academy @Home](http://eubias.org/NEUBIAS/training-schools/neubias-academy-home/) - Live online events targeting all levels in bioimage analysis
* [Code Refinery online workshops](https://coderefinery.org/workshops/upcoming/) - interactive online workshops teaching reproducible research practices

## Online course platforms

* [edX](https://www.edx.org/) - meta e-learning platform that aggregates courses from several e-learning platforms on a wide range of topics.
* [Khan Academy](https://www.khanacademy.org/) - mostly known for its math lessons, now also includes science, engineering and computation.

## Programming/algorithmic challenges

* [Rosalind](http://rosalind.info) - a platform for learning bioinformatics and programming through problem solving
* [Project Euler](https://projecteuler.net/) - provides a series of challenging mathematical/computer programming problems that will require more than just mathematical insights to solve
* [Advent of Code](https://adventofcode.com/) - solve a programming challenge for every day in December - challenges from previous years are also available.

## Resources for young ones

* [Bioinformatics at schools](http://en.bioinformatica-na-escola.org/) - a project that brings bioinformatics to secondary education students (slightly old, some content may be outdated with online resources)

This page is maintained by the Bio-IT Group [on the EMBL GitLab](https://git.embl.de/grp-bio-it/online-learning-resource-recommendations/). We'd love to receive more recommendations from our community members. To add a resource to this page, please follow the instructions [here](https://git.embl.de/grp-bio-it/online-learning-resource-recommendations/master/README.md) or [send us an email](mailto:bio-it@embl.de).