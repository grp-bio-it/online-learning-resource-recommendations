# online-learning-resource-recommendations

The recommendations listed in [`online-learning.md`](./online-learning.md) will be 
[displayed on the equivalent page][bio-it-online-learning] 
on the [Bio-IT site][bio-it-home].

More recommendations are welcome!
To submit your own recommendation,
provide us with the details via one of the following methods:

1. __Submit a Merge Request to this repository__ - this is the most direct way and our preferred option
2. __Tell us about it in an Issue on this repository__
3. __Send it by email to [`bio-it@embl.de`](mailto:bio-it@embl.de)__

[bio-it-home]: https://bio-it.embl.de/
[bio-it-online-learning]: https://bio-it.embl.de/online-learning/
